---
# See `man pandoc | grep "Metadata variables"`
title: DIN SPEC 3105-2
# These two will be supplied their final value from `git describe`
subtitle: "version: <UNKNOWN>"
version: <UNKNOWN>
lang: en-US
charset: UTF-8
license: CC-BY-SA 4.0
keywords: Open source hardware, DIN, Specification, 3105-2
subject: Specification
category: Norm
papersize: a4
geometry: "top=2cm,bottom=2cm,left=3cm,right=3cm"
description: English draft of the Deutsche Industrie Norm for open source hardware - community-based assessment
---

Open Source Hardware –
Part 2: Community-based assessment

Open Source Hardware –
Teil 2: Community-basierte Bewertung

Open Source Hardware –
Partie 2: Évaluation communautaire

# Foreword

This DIN SPEC is part of a DIN pilot project for the cooperation with
open source communities.

The content of this document (both parts of DIN SPEC 3105) is published
under Creative Commons Attribution-ShareAlike 4.0 License
(international) (CC-BY-SA 4.0,
<https://creativecommons.org/licenses/by-sa/4.0/legalcode>) by DIN e.V.
For attribution, the authors (DIN SPEC 3105 Workshop, named in the
foreword) may be represented by \"DIN e.V.\".

The pilot project elaborates the possibilities to publish DIN SPECs
under a free/open license, which could enable the cooperation with open
source communities. Since the publication under CC-BY-SA 4.0 allows
everyone to use, share and adapt the content of this document under the
same or compatible licenses, the maintenance procedure of such a DIN
SPEC needs to be elaborated during the pilot project.

This document is the first edition released by DIN and has the version
number 0.10 given by the open source hardware community. The document
will be shared on <https://din.one> and
<https://gitlab.com/OSEGermany/OHS/> for further revision based on
external contributions.

This DIN SPEC has been developed according to the PAS procedure. The
development of a DIN SPEC according to the PAS procedure is carried out
in DIN SPEC (PAS)-consortiums and does not require the participation of
all stakeholders.

This document has been developed and adopted by the initiator(s) and
authors named below:

- Felix Arndt\
  Open Source Imaging and Fair GmbH

- Dr. Jérémy Bonvoisin\
  University of Bath

- Tobias Burkert, Lukas Schattenhofer\
  TU Berlin, Student

- Jerry de Vos\
  Precious Plastic

- Fabian Flüchter\
  IP Center Bucerius Law School

- Martin Häuer, Dietrich Jäger, Timm Wille\
  Open Source Ecology Germany e.V.

- Mehera Hassan, Robert Mies\
  TU Berlin, Institute for Machine Tools and Factory Management, Chair of Quality Science
  
- Brynmor John\
  Field Ready

- Manuel Moritz, Dr. Tobias Redlich\
  Helmut Schmidt University

- Christian Schmidt-Gütter\
  Cradle to Cradle e.V.

- Emilio Velis\
  Appropedia Foundation

- Joost van Well\
  Cleopa GmbH

- Diderik van Wingerden\
  think.innovation

- Tobias Wenzel\
  Journal of Open Hardware

- Dr. Lukas Winter\
  Physikalisch Technische Bundesanstalt
  
- Lars Zimmermann\
  Mifactori – Open Circularity

At present, there are no standards covering this topic.

DIN SPEC (PAS)s are not part of the body of German Standards.

A draft of this DIN SPEC (PAS) has not been published.

Despite great efforts to ensure the accuracy, reliability and precision
of technical and non-technical information, the DIN SPEC
(PAS)-consortium cannot give any explicit or implicit assurance or
warranty in respect of the accuracy of the document. Users of this
document are hereby made aware that the the consortium cannot be held
liable for any damage or loss. The application of this DIN SPEC (PAS)
does not release users from the responsibility for their own actions and
is applied at their own risk.

Attention is drawn to the possibility that some of the elements of this
document may be the subject of patent rights. DIN shall not be held
responsible for identifying any or all such patent rights.

Provision of this document free of charge as a PDF via the Beuth WebShop
has been financed in advance.

For current information on this standard, please go to DIN's website
([www.din.de](http://www.din.de)) and search for the document number in
question.

# Introduction

This standard defines a community-based assessment procedure enabling
open source hardware (OSH) originators to make trustable claims
regarding the compliance of their creations with the requirements of the
DIN SPEC 3105-1. It supports the consistent and transparent labelling of
OSH and helps building the necessary trust to enable a more mainstream
adoption of the principles of open source in the creation of physical
artefacts.

The community-based assessment is not equivalent to an ISO/IEC 17067
certification scheme. Therefore the requirements on community-based
assessment should not be compared to a usual certification scheme.

The concept of open source is rooted in values of transparency and
participative governance. This standard defines an assessment process
that aligns with these values and builds upon the open review process
model at work in academic publication. It sets the requirements for an
online process enabling any interested person to contribute to the
assessment of a piece of hardware and to support best practices of OSH.

Alongside with DIN SPEC 3105-1 "Open source hardware --- Part 1:
Requirements for technical documentation"^[\[1\]](#bib-din-spec-3105-1)^ this standard is the
first standard published by DIN e.V. under a free/open license.
Following the principles of open source, anybody can contribute to its
further development online. Please refer to
<https://gitlab.com/OSEGermany/OHS> to review the current state of
ongoing processes and to contribute.

# 1 Scope

This document defines requirements for implementing a community-based
assessment procedure for open source hardware. It aims at groups or
persons willing to build an assessment procedure as well as groups or
persons willing to attest the compliance of the documentation of a piece
of hardware with the requirements set in the DIN SPEC 3105-1^[\[1\]](#bib-din-spec-3105-1)^.

# 2 Normative references

The following documents are referred to in the text in such a way that
some or all of their content constitutes requirements of this document.
For dated references, only the edition cited applies. For undated
references, the latest edition of the referenced document (including any
amendments) applies.

DIN SPEC 3105‑1:2020-07, 
*Open Source Hardware – 
Requirements for technical documentation*

E DIN EN ISO/IEC 17000:2019-05, 
*Konformitätsbewertung – 
Begriffe und allgemeine Grundlagen (ISO/IEC DIS 17000:2019; Deutsche und Englische Fassung prEN ISO/IEC 17000:2019*

DIN EN ISO/IEC 17065:2013-01, 
*Konformitätsbewertung – 
Anforderungen an Stellen, die Produkte, Prozesse und Dienstleistungen zertifizieren (ISO/IEC 17065:2012); Deutsche und Englische Fassung EN ISO/IEC 17065:2012*

# 3 Terms and definitions

For the purposes of this document, the [terms and definitions given in DIN SPEC 3105-1](DIN_SPEC_3105-1.md#3-terms-and-definitions) and the following apply.

DIN and DKE maintain terminological databases for use in standardization
at the following addresses:

- DIN-TERMinologieportal: available at
  <https://www.din.de/go/din-term>

- DKE-IEV: available at
  <http://www.dke.de/DKE-IEV>

## 3.1 conformity assessment body

::: {.Definition}
__body that performs conformity assessment activities, excluding accreditation__
:::

Note 1 to entry: 
A conformity assessment body is a natural or legal person that

1.  maintains a process and IT infrastructure that fully supports the
    implementation of the community-based assessment process defined in
    this standard;;

2.  publicly acknowledges the compliance of piece of hardware
    ([DIN SPEC 3105-1:2020-07, 3.1](DIN_SPEC_3105-1.md#31-piece-of-hardware)) with the requirements set in the
    DIN SPEC 3105-1 based on information gathered in their self-hosted
    assessment process;

3.  ensures access to all attested documentation releases
    ([DIN SPEC 3105-1:2020-07, 3.9](DIN_SPEC_3105-1.md#39-documentation-release)), *reviews* ([3.3](#33-review)), *attestations*
    ([3.5](#35-attestation)) and *complaints* ([3.6)](#36-complaint))), regardless of their formal validity,
    under a free/open license ([DIN SPEC 3105-1:2020-07, 3.2](DIN_SPEC_3105-1.md#32-free-license-open-license)).

Note 2 to entry: 
It is under the own responsibility of the conformity
assessment body to maintain compliance with all requirements stated in
the present standard.

\[SOURCE: E DIN EN ISO/IEC 17000:2019-05, 2.1.6 modified -- note 1 and 2
to entry added\]

## 3.2 client

::: {.Definition}
__organization or person responsible to a *conformity assessment body* ([3.1](#31-conformity-assessment-body)) for ensuring that *assessment requirements* (DIN EN ISO/IEC 17065:2013-01, 3.7), are fulfilled__
:::

Note 1 to entry: 
Within this standard "assessment requirements" are the
requirements stated in the [DIN SPEC 3105-1](DIN_SPEC_3105-1.md).

Note 2 to entry: 
A client is not necessarily a originator of the piece
of hardware ([DIN SPEC 3105-1:2020-07, 3.1](DIN_SPEC_3105-1.md#31-piece-of-hardware)) or an author of the
corresponding documentation release ([DIN SPEC 3105-1:2020-07, 3.9](DIN_SPEC_3105-1.md#39-documentation-release)). The
client has however sufficient resources to support the application along
the assessment process (e.g. as point of contact for the *conformity
assessment body* ([3.1](#31-conformity-assessment-body))).

\[SOURCE: DIN EN ISO/IEC 17065:2013-01, 3.1 modified – certification
body changed to conformity assessment body; certification requirement
changed to assessment requirement; removed "including product
requirements (3.8)"; note 1 and 2 to entry added\]

## 3.3 review

::: {.Definition}
__consideration of the suitability, adequacy and effectiveness of selection and determination activities, and the results of these activities, with regard to fulfilment of specified requirements (DIN EN ISO/IEC 17000:2019‑05, 2.2.1) by an object of conformity assessment (DIN EN ISO/IEC 17000:2019-05, 2.1.2).__
:::

Note 1 to entry: 
Within this standard the "object of conformity
assessment" is a documentation release ([DIN SPEC 3105-1:2020-07, 3.9](DIN_SPEC_3105-1.md#39-documentation-release)) or
a defined part of it; "specified requirements" are the requirements
stated in the [DIN SPEC 3105-1](DIN_SPEC_3105-1.md). In other words, a review checks the
compliance of a documentation release ([DIN SPEC 3105-1:2020-07, 3.9](DIN_SPEC_3105-1.md#39-documentation-release))
over all requirements of the DIN SPEC 3105-1.

Note 2 to entry: 
A review is a digital document.

Note 3 to entry: 
A reviewer is:

1.  a natural person that is neither client nor author of the
    documentation release ([DIN SPEC 3105-1:2020-07, 3.9](DIN_SPEC_3105-1.md#39-documentation-release)) and has no
    conflict of interest regarding the outcome of the assessment
    process;

2.  a recipient of the documentation release ([DIN SPEC 3105-1:2020-07, 3.9](DIN_SPEC_3105-1.md#39-documentation-release)) he/she reviews.

It is the own responsibility of the reviewer to make an honest statement
regarding their belonging to the group of recipients ([DIN SPEC 3105-1:2020-07, 3.7](DIN_SPEC_3105-1.md#37-recipients) and about their absence of conflict of interest
regarding the outcome of the assessment process. The *conformity
assessment body* ([3.1](#31-conformity-assessment-body)) may ask reviewers to make a public declaration on
their conflicts of interest.

\[SOURCE: E DIN EN ISO/IEC 17000:2019-05, 2.4.1 modified -- Note 1, 2 and 3 to entry added\]

## 3.4 decision

::: {.Definition}
__conclusion based on the results of *review* ([3.3](#33-review)), that fulfilment of specified requirements (DIN EN ISO/IEC 17000:2019-05, 2.2.1) has or has not been demonstrated.__
:::

Note 1 to entry: 
Within this standard "specified requirements" are the requirements stated in the [DIN SPEC 3105-1](DIN_SPEC_3105-1.md).

Note 2 to entry: 
Decision is made by reviewers.

\[SOURCE: E DIN EN ISO/IEC 17000:2019-05, 2.4.2 modified – note 1 and 2 to entry added.\]

## 3.5 attestation

::: {.Definition}
__issue of a statement, based on a *decision* ([3.4](#34-decision)) that fulfilment of specified requirements (DIN EN ISO/IEC 17000:2019-05, 2.2.1) has been demonstrated__
:::

Note 1 to entry: 
The resulting statement, referred to in this document
as a "statement of conformity", is intended to convey the assurance that
the specified requirements have been fulfilled. Such an assurance does
not, of itself, afford contractual or other legal guarantees.

Note 2 to entry: 
Within this standard "specified requirements" are the
requirements stated in the [DIN SPEC 3105-1](DIN_SPEC_3105-1.md).

\[SOURCE: E DIN EN ISO/IEC 17000:2019-05, 2.4.3 modified – note 2 to entry replaced.\]

## 3.6 complaint

::: {.Definition}
__expression of dissatisfaction, other than appeal (DIN EN ISO/IEC 17000:2019-05, 2.5.6), by any person or organization to a *conformity assessment body* ([3.1](#31-conformity-assessment-body)), relating to the activities of that body, where a response is expected__
:::

Note 1 to entry: 
A complaint may, among others, be triggered in case:

-   the documentation release ([DIN SPEC 3105-1:2020-07, 3.9](DIN_SPEC_3105-1.md#39-documentation-release)) is not
    accessible anymore or parts of it disappeared,

-   the licensing terms have been changed and are not compliant anymore
    with the requirements of the [DIN SPEC 3105-1](DIN_SPEC_3105-1.md) or

-   any other relevant information has been changed or disappeared.

\[SOURCE: E DIN EN ISO/IEC 17000:2019-05, 2.5.7 modified – or
accreditation body (2.1.7) removed; note 1 to entry added\]

# 4. Symbols and abbreviations

OSH&nbsp;&nbsp;&nbsp;&nbsp;Open Source Hardware

TsDC&nbsp;&nbsp;&nbsp;Technology-specific Documentation Criteria

# 5. Community-based assessment

## 5.1. General requirements

All actions performed by conformity assessment bodies, clients,
reviewers or individuals or organizations submitting complaints in the
context of procedures defined in section [5.2](#52-issue-and-challenge-an-attestation) are performed online and
are publicly visible. This means that the information relative to these
actions (e.g. date, author, content) can be viewed online by anyone
without any restricted access and is released under a free/open license
([DIN SPEC 3105-1:2020-07, 3.2](DIN_SPEC_3105-1.md#32-free-license-open-license)). This information is at the earliest
visible when the action has been performed and at the latest when the
conformity assessment body decides upon the delivery of an attestation.
The attestation automatically becomes void in the moment and for the
period of time where the access to this information becomes restricted
or lapses.

Along review processes, some inadequacies in the definition of TsDC may
come into light. These should be reported by the conformity assessment
body in order to motivate the development of a new version of the
concerned TsDC.

## 5.2. Issue and challenge an attestation

### 5.2.1 Application

Once the conformity assessment body receives an application, it opens
the assessment process by sending a written confirmation to the client
and accepts corresponding reviews from this moment on.

An application includes:

1.  name and contact details of the client;

2.  the permanent URL to the documentation release;

3.  the application date.

The reviewing process is generally community-based and for testing the
compliance of the documentation release to the requirements stated in the
DIN SPEC 3105-1. However, the conformity assessment body may support the
assessment process; this includes requesting reviews from individuals
selected by the conformity assessment body.

### 5.2.2 Reviews and decisions

A review includes:

1.  an unambiguous reference to the corresponding documentation release;

2.  a mention of the documents that have been reviewed;

3.  an unambiguous reference to the reviewer (e.g. full name and contact
    details);

4.  textual comments from the reviewer justifying his/her decision;

5.  the decision of the reviewer.

The reviewer can take two mutually exclusive decisions:

a)  Approval, when the performed review confirmed the compliance of the
    submitted documentation release with the requirements of the DIN
    SPEC 3105-1.

b)  Approval subject to revision, when the review identified
    misalignment of the submitted documentation release with the
    requirements of the DIN SPEC 3105-1 that need to be clarified prior
    to final approval.

Misalignments can be clarified through two routes:

  i.  the client submits a revised documentation release that is
      reassessed and approved by the reviewer;

  ii. the client submits sufficient arguments to correct an eventual
      misinterpretation of the reviewer.

### 5.2.3 Attestation

An attestation is issued as valid when the submitted documentation
release has been completely approved at least twice by decisions.

Reviews and complaints from already attested documentation releases may
be valid for the assessment process when the referred parts of the
documentation are identical.

An attestation:

1.  makes a mention of its state which is either valid or void;

2.  clearly identifies the conformity assessment body as its author;

3.  is accessible through a permanent URL;

4.  bears a release date;

5.  includes the permanent URL to the corresponding documentation
    release;

6.  contains all corresponding reviews and unresolved complaints or
    includes permanent URLs to them.

Annex A gives an example to communicate the compliance of a
documentation release with the DIN SPEC 3105-1 via a label.

After an attestation is issued the reviewing process is closed and
further decisions will not affect the state of the attestation.

However, an attestation can be challenged by complaints. The conformity
assessment body may void the attestation and reopen the reviewing
process in order to clarify the misalignments stated in the complaints.
When the clarification of the misalignments has been confirmed at least
twice by decisions the attestation regains its valid state and the
reviewing process is closed again.

EXAMPLE Client submits a documentation release to a conformity
assessment body and it gets approved by reviewer A and reviewer B. Now a
reviewer C identifies misalignments. The conformity assessment body has
practically two options then:

a)  moderate the discussion between client and the three reviewers until
    all misalignments are resolved, then issue a valid attestation;

b)  issue a valid attestation, since the submitted documentation release
    has been approved twice. Reviewer C can now submit a complaint to
    the conformity assessment body.

Option a is recommended as best practice. Regarding option 2: As
according to the general requirements ([5.1](#51-general-requirements)) all actions are publicly
visible and information is released under a free/open license, an
unprocessed, but justified complaint may cause rejection within the open
source hardware community, on which this attestation process is based.
The open source hardware community may also copy all or a selection of
the information published under a free/open license in the history of
that conformity assessment body, creating a new conformity assessment
body. In software development, this would be called a fork.

# 6. Annex A - Label and specifications

The label shown in [Figure A.1](#fig-a1) provides a way for conformity assessment bodies 
to communicate the compliance of a documentation release with the DIN SPEC 3105-1.

<a name="fig-a1">

![Label sample for an attestation
](label-sample.svg){width="5.98cm"}

</a>

The label contains:

1.  the phrase "Open Source Hardware according to DIN SPEC
    3105-1:2020-07";

2.  the name of the conformity assessment body delivering the label;

3.  the permanent URL linking to the attestation of the conformity
    assessment body.

Formal specifications:

The label shown in [Figure A.1](#fig-a1) is monochrome black (hex code \#000000)
and consists of a gear logo and text.

The gear logo, "[Open Source Hardware Logo](https://www.eevblog.com/oshw/)" by [Macklin Chaffee](http://macklinchaffee.com/), is used
under CC-BY-SA 4.0^[\[2\]](#bib-cc-by-sa-4)^; all text has been removed from the original and
the colour has been changed to black.

Measure *a* is the width of one tooth of the gear logo without rounding.

The font, "[Open Sans](https://fonts.google.com/specimen/Open+Sans)" by Steve Matteson is used under Apache License,
Version 2^[\[3\]](#bib-apl-2)^.

The text is subject to the following formal specifications:

1.  font style: bold for "Open Source Hardware", semibold for the rest
    of the text;

2.  font size: half of measure *a*;

3.  centred to the vertical symmetry axis of the gear logo;

4.  line spacing: single;

5.  spacing under paragraph: quarter of measure *a*.

# Bibliography

1. \phantomsection \hypertarget{bib-din-spec-3105-1}{}<a name="bib-din-spec-3105-1"></a>
    [DIN SPEC 3105‑1:2020-07, *Open Source Hardware – Teil 1: Requirements for technical documentation*](DIN_SPEC_3105-1.md)
    
2. \phantomsection \hypertarget{bib-cc-by-sa-4}{}<a name="bib-cc-by-sa-4"></a>
    [Creative Commons Corporation, "Attribution ShareAlike 4.0 International," 25-Nov-2013 \[Online\]. Available: <https://creativecommons.org/licenses/by-sa/4.0/> \[Accessed: 31-Mar-2020\]](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

3. \phantomsection \hypertarget{bib-apl-2}{}<a name="bib-apl-2"></a>
    [Apache Software Foundation, "Apache License, Version 2," Jan-2004 \[Online\]. Available: <https://www.apache.org/licenses/LICENSE-2.0.html> \[Accessed: 31-Mar-2020\]](http://www.apache.org/licenses/LICENSE-2.0)
