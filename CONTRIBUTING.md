---
# See `man pandoc | grep "Metadata variables"`
title: Contributing
# These two will be supplied their final value from `git describe`
subtitle: "version: <UNKNOWN>"
version: <UNKNOWN>
lang: en-US
charset: UTF-8
license: CC-BY-SA 4.0
subject: Contribution guidelines
papersize: a4
geometry: "top=2cm,bottom=2cm,left=3cm,right=3cm"
---

# Contributing to Open Hardware Standards

**Welcome to the community :)**\
With this standard we aim to contribute to the infrastructure for open source hardware. 
We feel, that's super important work and it often requires lots of patience, concentration and creativity.\
That works much better in a team. So happy to have you here. **You are very welcome!**

**General remark:**

1. You are all smart adults. If you don't find the specification you are looking for in this document, please ask others or do it to the best of your knowledge.
2. Open documentation is key. Issues, discussions, decisions and changes are public and always open for feedback. This way, it's hard to exclude perspectives from other stakeholders and documents are highly reviewed from an early stage on.
3. Thanks for adding value! Contstructive feedback, issue reporting, remarks, ideas… standards are the condensation of cumulated knowledge. We need **your** ingenuity to keep them alive and up-to-date. And we are here, ready to assist you when you have trouble joining the process.


## Roles

- consortium lead (lead & co-lead; 1…3 individuals; **owner rights**) 
- consortium (**developer rights**)
- community (**reporter rights**)
- admin (1+ individuals; **maintainer rights**)

## Contribution flow

### Content changes

1. Any of the roles mentioned above may contribute via issues, merge requests or other tasks.
2. All relevant discussions must be documented in this repo. If you had an issue discussed via mail, copy/summarise it in the corresponding issue/merge request or open a new one. This gitlab repo is _the_ point of reference for all relevant information that led to the content in the document.
3. Changes in the document happen via merge request. 
4. Consortium decides on merge requests. Consortium lead is part of the consortium. Decisions must be taken in meetings, open to all consortium members. Meetings are moderated by consortium lead. As any relevant discussion, meeting minutes are published in this gitlab repo in MEETUS.MD. Decisions should be taken in consent with all consortium members. However, if not possible or feasible (consortium members not present in the meeting, hardliners, issues on a personal level etc.) the consortium lead may place a decision, even without the agreement of all consortium members. Consortium members are free to open a new branch with a document that fully reflects their perspective.
4. Document may be sent to DIN e.V. to transform it into an official standard. From there on, please follow DIN's contribution guidelines.
5. Only restrict/formalise what's definitely necessary. It's a pilot project.

### Issue management

1. Anyone may create an issue.
2. Any community or consortium member may label the issues accordingly so that specialist are encouraged to give input to the discussion.
3. Every consortium member is assigned to at least one label (and vice versa). These assignments should reflect the field of profession/interest of the consortium member and give a guideline for the responsibility of open issues. So if there's an unresolved issue and it carries your label, please have a look at it.

## Version number convention

Documents here are versioned in compliance with [Semantic Versioning 2.0.0](https://github.com/semver/semver). The following section is an extract of this definition.

Given a version number **MAJOR.MINOR.PATCH**, increment the:

- **MAJOR** version when backwards incompatible changes are made,
- **MINOR** version when functionality in a backwards compatible manner have been added,
- **PATCH** version when backwards compatible bug fixes are made.
- **PATCH** version _must_ be incremented if only backwards compatible bug fixes are introduced. A bug fix is defined as an internal change that fixes incorrect behavior.

**MINOR** version +must be incremented if new, backwards compatible functionality is introduced. It +must be incremented if any section/functionality is marked as deprecated. It _may_ include **PATCH** level changes. PATCH version +must be reset to 0 when **MINOR** version is incremented.

**MAJOR** version _must_ be incremented if any backwards incompatible changes are introduced. It _may_ also include **MINOR** and **PATCH** level changes. **PATCH** and **MINOR** version _must_ be reset to 0 when **MAJOR** version is incremented.

## Communication

- We still rely on eMail. However, we are open for suggestions.
- We generally meet online and [here](https://fairmeeting.net/OSHstandardization).
- Find the meeting minutes in this repos [MEETUS.md](MEETUS.md).


