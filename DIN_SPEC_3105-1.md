---
# See `man pandoc | grep "Metadata variables"`
title: DIN SPEC 3105-1
# These two will be supplied their final value from `git describe`
subtitle: "version: <UNKNOWN>"
version: <UNKNOWN>
lang: en-US
charset: UTF-8
license: CC-BY-SA 4.0
keywords: Open source hardware, DIN, Specification, 3105-1
subject: Specification
category: Norm
papersize: a4
geometry: "top=2cm,bottom=2cm,left=3cm,right=3cm"
description: English draft of the Deutsche Industrie Norm for open source hardware - requirements for technical documentation
---

Open Source Hardware — 
Part 1: Requirements for technical documentation

Open Source Hardware — 
Teil 1: Anforderungen an die technische Dokumentation

Open Source Hardware — 
Partie 1: Exigences relatives à la documentation technique

# Foreword

This DIN SPEC is part of a DIN pilot project for the cooperation
with open source communities.

The content of this document (both parts of DIN SPEC 3105) is 
published under Creative Commons Attribution-ShareAlike 4.0 
License (international) (CC-BY-SA 4.0, 
<https://creativecommons.org/licenses/by-sa/4.0/legalcode>) 
by DIN e.V. For attribution, the authors (DIN SPEC 3105 Workshop, 
named in the foreword) may be represented by \"DIN e.V."\.

The pilot project elaborates the possibilities to publish 
DIN SPECs under a free/open license, which could enable the 
cooperation with open source communities. Since the publication 
under CC-BY-SA 4.0 allows everyone to use, share and adapt 
the content of this document under the same or compatible licenses, 
the maintenance procedure of such a DIN SPEC needs 
to be elaborated during the pilot project.

This document is the first edition released by DIN and has 
the version number 0.10 given by the open source hardware community. 
The document will be shared on <https://din.one> and 
<https://gitlab.com/OSEGermany/OHS/> for further revision based 
on external contributions.This DIN SPEC has been developed 
according to the PAS procedure. The development of a DIN SPEC 
according to the PAS procedure is carried out in 
DIN SPEC (PAS)-consortiums and does not require the 
participation of all stakeholders.

This document has been developed and adopted by the initiator(s) and authors named below:

- Felix Arndt\
  Open Source Imaging and Fair GmbH

- Dr. Jérémy Bonvoisin\
  University of Bath

- Tobias Burkert, Lukas Schattenhofer\
  TU Berlin, Student

- Jerry de Vos\
  Precious Plastic

- Fabian Flüchter\
  IP Center Bucerius Law School

- Martin Häuer, Dietrich Jäger, Timm Wille\
  Open Source Ecology Germany e.V.

- Mehera Hassan, Robert Mies\
  TU Berlin, Institute for Machine Tools and Factory Management, Chair of Quality Science
  
- Brynmor John\
  Field Ready

- Manuel Moritz, Dr. Tobias Redlich\
  Helmut Schmidt University

- Christian Schmidt-Gütter\
  Cradle to Cradle e.V.

- Emilio Velis\
  Appropedia Foundation

- Joost van Well\
  Cleopa GmbH

- Diderik van Wingerden\
  think.innovation

- Tobias Wenzel\
  Journal of Open Hardware

- Dr. Lukas Winter\
  Physikalisch Technische Bundesanstalt
  
- Lars Zimmermann\
  Mifactori – Open Circularity

At present, there are no standards covering this topic.

DIN SPEC (PAS)s are not part of the body of German Standards.

A draft of this DIN SPEC (PAS) has not been published.

Despite great efforts to ensure the accuracy, reliability and precision 
of technical and non-technical information, the DIN SPEC (PAS)-consortium 
cannot give any explicit or implicit assurance or warranty in respect 
of the accuracy of the document. Users of this document are hereby 
made aware that the the consortium cannot be held liable for any 
damage or loss. The application of this DIN SPEC (PAS) does not 
release users from the responsibility for their own actions and 
is applied at their own risk.

Attention is drawn to the possibility that some of the elements 
of this document may be the subject of patent rights. DIN shall 
not be held responsible for identifying any or all such patent rights.

Provision of this document free of charge as a PDF via 
the Beuth WebShop has been financed in advance.

For current information on this standard, please go to DIN’s website 
(<www.din.de>) and search for the document number in question.

# Introduction

An increasing number of initiatives from grassroots, academic and 
business communities adopt the practice of publicly releasing the design 
files of products they developed^[\[1\]](#bib-how-collaborative)^. 
Altogether, these initiatives framed the concept of open source 
hardware (OSH) that extends the well-established approach to 
intellectual property management in open source software to the realms 
of physical artifacts^[\[2\]](#bib-democratizing-production)^. This implements an alternative approach to 
conventional product development that bears a formidable potential for 
organizational and business innovation^[\[3\]](#bib-bottom-up-economics)^.

The process of charting a consistent identity based on commonly 
acknowledged definitions and assessable criteria is made difficult by 
the multifactorial and partly ill-defined nature of openness and freedom 
once applied to physical products^[\[4\]](#bib-open-o-meter)^. In practice, the presupposed 
information disclosure is applied in very different ways, resulting from 
a large spectrum of interpretations of the concept of OSH^[\[5\]](#bib-the-source)^. This 
document aims to provide a definition that delivers clear criteria 
allowing to distinguish what is OSH from what it is not. It is the 
result of a standardization process involving major actors in the field 
and has been the object of a public consultation.

A significant part of the effort to standardize practices in OSH in the past 
has been performed by the Open Source Hardware Association (OSHWA). 
This initiative issued the most widely acknowledged definition of OSH^[\[6\]](#bib-ohw-principles)^ 
and published a comprehensive set of best practices^[\[7\]](#bib-ohw-best-practices)^. 
Since 2016, it offers a self-certification scheme for OSH originators 
to signpost their compliance with this definition. 
The OSHWA Definition 1.0 states^[\[6\]](#bib-ohw-principles)^:

“Open source hardware is hardware whose design is made publicly 
available so that anyone can study, modify, distribute, make, and sell 
the design or hardware based on that design.”

Despite the significant contribution it made to the field of OSH, this 
definition only focuses on the licensing aspects of product-related 
information disclosure and does not set concrete requirements regarding 
the content of the information to be disclosed. Thus it defines what 
“open” means in the context of technical documentation, but a widely 
acknowledged reference stating what minimal set of information 
constitutes the “source” of OSH is yet missing.

The present document addresses this gap by extending the OSHWA 
Definition 1.0^[\[6\]](#bib-ohw-principles)^, which itself extends the Open Source Definition^[\[8\]](#bib-os-definition)^. It acknowledges that OSH is not only a matter of licensing but 
also a matter of documentation contents.

By setting a frame for the documentation of OSH, the authors hope to 
support the consistent and transparent labelling of OSH, to contribute 
to the standardization and improvement of OSH practices, and ultimately 
to enable a more mainstream adoption of the principles of open source in 
the creation of physical artefacts.

Alongside with DIN SPEC 3105-2 “Open source hardware — Part 2: Community-based assessment”^[\[9\]](#bib-din-spec-3105-2)^ 
this standard is the first standard published by DIN e.V. under a free/open license. 
Following the principles of open source, anybody can contribute to its 
further development online. Please refer to 
<https://gitlab.com/OSEGermany/OHS> to review the current state 
of ongoing processes and to contribute.

# 1. Scope

This standard sets requirements for technical documentation. It is
designed to be complementary to existing standards and guidelines for
technical documentation (e.g. VDI 4500) and is not aimed at superseding
them. It focuses on the aspects of the technical documentation that are
related to its compliance with the principles of open source.

The principles and definitions provided by the DIN SPEC 3105-1 "Open
source hardware – Part 1: Requirements for technical documentation"
set a frame for the DIN SPEC 3105-2 "Open source hardware – Part 2:
Community-based assessment"^[\[9\]](#bib-din-spec-3105-2)^ which in turn sets concrete and
practical requirements for the establishment of assessment procedures.

This document builds upon the OSHWA Definition 1.0^[\[6\]](#bib-ohw-principles)^ and the Open
Source Definition^[\[8\]](#bib-os-definition)^.

# 2. Normative references

There are no normative references in this document.

# 3. Terms and definitions

For the purposes of this document, the following terms and definitions
apply.

DIN and DKE maintain terminological databases for use in standardization
at the following addresses:

- DIN-TERMinologieportal: available at
  <https://www.din.de/go/din-term>

- DKE-IEV: available at 
  <http://www.dke.de/DKE-IEV>

## 3.1 piece of hardware

::: {.Definition}
__any discrete (i.e. countable) physical artefact__
:::

EXAMPLE A machine, a device, a piece of equipment, or any other tangible
object.

Note 1 to entry: A piece of hardware can be either a freestanding single
component, an assembly that includes two or more components, or a
component belonging to an assembly.

Note 2 to entry: The term piece of hardware indifferently refers to a
unique physical artefact (e.g. a one-off prototype) or to the concept of
a physical artefact that has been physically realized one or more times
(e.g. a product model produced in series). Concepts of physical
artefacts do not qualify as pieces of hardware as long as they have not
been realized at least once.

Note 3 to entry: This document refers to physical objects as pieces of
hardware since the word "hardware" is not countable in English. It is
thus incorrect to write "a hardware". Because this document refers to
specific products, prototypes, or artefacts that qualify as OSH, it
needs to make the word "hardware" countable. Using "piece of" allows to
make of "hardware" a countable quantity. It makes it possible to write
sentences like "how many pieces of hardware have been certified?"

## 3.2. free license, open license

::: {.Definition}
__license agreement that grants anyone with the right to reuse another creator's work, giving four major freedoms: use, study, modify and distribute__
:::

EXAMPLE
CC-BY 4.0^[\[10\]](#bib-cc-by-4)^,
CC-BY-SA 4.0^[\[11\]](#bib-cc-by-sa-4)^,
GPLv3^[\[12\]](#bib-gpl-3)^,
CERN OHL v2^[\[13\]](#bib-ohl-2)^

Note 1 to entry: 
In this standard, a license complying with the
requirements of at least one of the following definitions is considered
a free/open license:

* Free Software Definition^[\[14\]](#bib-fsf-definition)^,

* Debian Free Software Guidelines^[\[15\]](#bib-debian-fs-guidelines)^,

* Open Source Definition^[\[8\]](#bib-os-definition)^,

* Definition of Free Cultural Works^[\[16\]](#bib-free-cultural-works-definition)^,

* The Open Definition^[\[17\]](#bib-open-definition)^,

* OSHWA Definition^[\[6\]](#bib-ohw-principles)^.

## 3.3 open source hardware

**OSH**

::: {.Definition}
__hardware for which a free right of any use belongs to the general public and whose *documentation* ([3.8](#38-documentation)) is completely available and freely accessible on the Internet__
:::

Note 1 to entry: 
A *piece of hardware* ([3.1](#31-piece-of-hardware)) is qualified as open source when
it\'s *documentation* ([3.8](#38-documentation))

1. has been released under licensing terms complying with the OSHWA
   Definition 1.0^[\[6\]](#bib-ohw-principles)^ (e.g. CERN OHL v2^[\[13\]](#bib-ohl-2)^) and therewith granting anyone
   with the *four rights of open source hardware* ([3.4](#34-four-rights-of-open-source-hardware));

2. provides enough information to enable *recipients* ([3.6](#37-recipients)) to exercise these
   rights.

Note 2 to entry: Such a *piece of hardware* ([3.1](#31-piece-of-hardware)) is therewith referred
to as a piece of open source hardware (OSH), may be certified under
terms defined by DIN SPEC 3105‑2, and labelled accordingly.

## 3.4 four rights of open source hardware

::: {.Definition}
__the *right to study* ([3.4.1](#341-right-to-study)), *to modify* ([3.4.2](#342-right-to-modify)), *to make* ([3.4.3](#343-right-to-make)), and *to distribute* ([3.4.3](#344-right-to-distribute))__
:::

Note 1 to entry: 
Granting these rights requires releasing the
*documentation* ([3.8](#38-documentation)) under a license complying with the requirements of
the OSWHA Definition 1.0^[\[6\]](#bib-ohw-principles)^. Exercising these rights is bound to
requirements regarding the content of the *documentation* ([3.8](#38-documentation)). The
four rights of open source hardware are detailed in the following
subsections.

Note 2 to entry: The four rights of open source hardware are a
translation of the "four essential freedoms" stated in the Free Software
Definition^[\[14\]](#bib-fsf-definition)^ to the context of physical artefacts. These freedoms
have been reinterpreted by the Open Source Hardware Association (OSHWA)
as the possibility to "study, modify, distribute, make, and sell"
hardware. This document reproduces the wording of the OSHWA, where
selling is seen as a way of distribution among others.

### 3.4.1 right to study

::: {.Definition}
__effective possibility to access sufficient information to understand the design rationale of the piece of *open source hardware* ([3.3](#33-open-source-hardware)) and its expected behaviour along its *life cycle* ([3.10](#310-life-cycle))__
:::

Note 1 to entry: The right to study includes access to the *documentation* ([3.8](#38-documentation))
of the considered piece of *open source hardware* ([3.3](#33-open-source-hardware)).

### 3.4.2 right to modify

::: {.Definition}
__effective possibility to edit the *documentation* ([3.8](#38-documentation)) and therewith to alter the design of the piece of *open source hardware* ([3.3](#33-open-source-hardware))__
:::

Note 1 to entry: The right to modify presupposes the *right to study* ([3.4.1](#341-right-to-study)).

### 3.4.3 right to make

::: {.Definition}
__effective possibility to operate all activities belonging to the *life cycle* ([3.10](#310-life-cycle)) of the piece of *open source hardware* ([3.3](#33-open-source-hardware))__
:::

EXAMPLE To manufacture the piece of *open source hardware* ([3.3](#33-open-source-hardware)), to operate it, to carry
out maintenance or to process it at its end-of-life.

Note 1 to entry: The right to make presupposes the *right to study* ([3.4.1](#341-right-to-study)).

### 3.4.4 right to distribute

right to distribute

::: {.Definition}
__effective possibility to give or sell the piece of open source hardware ([3.3](#33-open-source-hardware)) made based on the original or a modified version of the documentation ([3.8](#38-documentation))__
:::

1.  the original or a modified version of the *documentation* ([3.8](#38-documentation)) and

2.  the piece of *open source hardware* ([3.3](#33-open-source-hardware)) made based on the original
    or a modified version of the *documentation* ([3.8](#38-documentation))

## 3.5 technology

::: {.Definition}
__category of production processes used to make the piece of *open source hardware* ([3.3](#33-open-source-hardware)) or a set of physical features embedded in a function of the piece of *open source hardware* ([3.3](#33-open-source-hardware))__
:::

## 3.6 technology-specific documentation criteria

**TsDC**

::: {.Definition}
__document that specifies requirements applying to the *documentation* ([3.8](#38-documentation)) of *open source hardware* ([3.3](#33-open-source-hardware)) from a given *technology* ([3.5](#35-technology))__
:::

## 3.7 recipients

::: {.Definition}
__group of people addressed by the *documentation* ([3.8](#38-documentation))__
:::

Note 1 to entry: 
This group is characterized by a common state of
knowledge and set of abilities enabling its members to use the
*documentation* ([3.8](#38-documentation)) in order to exercise the four rights of *open
source hardware* ([3.3](#33-open-source-hardware)). By default, the recipients are specialists in
the fields of *technologies* ([3.5](#35-technology)) embedded in the piece of *open source
hardware* ([3.3](#33-open-source-hardware)), i.e. the persons skilled in the art corresponding to
this *technology* ([3.5](#35-technology)). The *documentation* ([3.8](#38-documentation)) can define recipients
alternatively to include a larger group of people. However, in any case,
the *documentation* ([3.8](#38-documentation)) provides no less information than what
specialists in the fields of *technologies* ([3.5](#35-technology)) embedded in the piece
of *open source hardware* ([3.3](#33-open-source-hardware)) would require exercising the four rights
of *open source hardware* ([3.3](#33-open-source-hardware)).

Note 2 to entry: 
In order to account for the contribution of multiple
specialists required by the development and production of complex pieces
of *open source hardware* ([3.3](#33-open-source-hardware), different recipients can eventually be
defined for different parts of the *documentation* ([3.8](#38-documentation)).

## 3.8 documentation

::: {.Definition}
__technical documentation; constitutes the "source code" of a piece of *open source hardware* ([3.3](#33-open-source-hardware))__
:::

## 3.9 documentation release

::: {.Definition}
__clearly identifiable version of the *documentation* ([3.8](#38-documentation)) that can be accessed in its entirety even after further modifications brought to the *documentation* ([3.8](#38-documentation))__
:::

## 3.10 life cycle

::: {.Definition}
__network of successive and parallel activities required or implied by the realization of the piece of *open source hardware* ([3.3](#33-open-source-hardware))__
:::

# 4 Symbols and abbreviations

OSH&nbsp;&nbsp;&nbsp;&nbsp;Open Source Hardware

TsDC&nbsp;&nbsp;&nbsp;Technology-specific Documentation Criteria

# 5 Requirements for technical documentation

## 5.1. General

The documentation is the set of digital documents enabling recipients to
execute all activities of the life cycle of a piece of OSH resulting
from the original contribution of the authors. The documentation of a
piece of OSH contains sufficient and necessary information for the
recipients to understand and operate all these activities without
requiring any external document that is not publicly available. Required
information can be partly provided by a reference to a publicly
available document as long as it provides no less information than
originally required.

The Documentation of a piece of OSH bears references of:

1.  its authors;

2.  its licensing terms;

3.  a functional description of the piece of OSH, e.g. what functions it
    is supposed to deliver, what is the problem it solves, for whom,
    etc.;

4.  a mention of all applying Technology-specific Documentation
    Criteria;

5.  all documents required by the mentioned Technology-specific
    Documentation Criteria;

6.  a name or working title given to the Piece of Hardware;

A piece of hardware can only qualify as OSH if all the technologies it
contains are covered by technology-specific documentation criteria.

In case a piece of hardware contains a technology that is not covered by
any TsDC yet, documentation is to be created to the best knowledge of
the authors.

NOTE
Paper documents do not qualify as documentation. The first reason
is that they are not editable and shareable in the same way as original
digital documents (without further processing). Making modifications to
paper documents requires reproducing the whole document. The second
reason is that OSH is an internet phenomenon and only really make sense
in this context.

## 5.2 Documentation release

A Documentation Release makes mention of:

1.  a release date;

2.  a release number unambiguously identifying a version of the
    documentation;

3.  a version of the piece of OSH to which the documentation release
    applies.

## 5.3 Access

The Documentation is deemed as accessible when:

-   it is published

-   in its original editable file format and,

-   in an export file format that

    -   is of well-established use in the corresponding field of
        technology,

    -   can be processed by software that is generally accessible to the
        recipients, and

    -   contains no less information than the original editable file
        format[^1].

-   the means of downloading it via the Internet is well-publicized[^2]
    and neither involve any charge or any moderation potentially
    conflicting with the principles of non-discrimination against
    persons or groups and non-discrimination against fields of
    endeavour.

-   the means of downloading it via the Internet is constantly active
    from the Release date and without interruption.

## 5.4 Valid Technology-specific documentation criteria

Valid TsDC:

-   clearly refer to and extends DIN SPEC 3105‑1 without superseding it;

-   provide technology-specific requirements for all phases of the Life
    Cycle;

-   are released under a free/open license;

-   appear in the list of approved TsDC in Annex A.

## 5.5 Lifecycle of Piece of Hardware

The following activities are part of the Lifecycle[^3]:

1.  Realization, spanning from raw material extraction, production of
    semi-finished products, final assembly. Activities in this phase are
    aimed at establishing the functionality of the piece of *open source
    hardware* (3.3).

2.  Operation and maintenance, including activities centered on
    delivering or maintaining the functionality of the piece of *open
    source hardware* (3.3)

3.  End-of-life, including reuse, refurbishment, reconditioning,
    recycling, disposal. Activities in this phase are aimed at making the
    physical components or a subset of functions available for other *pieces
    of hardware* (3.1).

# Annex A

(informative)

## Approved TsDC

The TsDC listed in table A.1 have been approved by the DIN SPEC 3105
consortium which created the present version of this standard. Unless
otherwise specified, always the latest release of these TsDC apply.

Table A.1 – Approved TsDC

| __Title__ | __issued by__      | __Reference/Repository__ |
| :-------- | :----------------- | :----------------------- |
| Open Hardware Standards – TsDC | Open Source Ecology Germany e.V. (2020) | <https://gitlab.com/OSEGermany/oh-tsdc>  |

# Bibliography

1.  \phantomsection \hypertarget{bib-how-collaborative}{}<a name="bib-how-collaborative"></a>
    [J. Bonvoisin, T. Buchert, M. Preidel, and R. Stark, "How collaborative is open source hardware? Insights from repository mining," *Design Science Journal*, 2018.](https://doi.org/10.1017/dsj.2018.15)

2. \phantomsection \hypertarget{bib-democratizing-production}{}<a name="bib-democratizing-production">
    [A. Powell, "Democratizing production through open source knowledge: from open software to open hardware," *Media, Culture & Society*, Aug. 2012.](https://doi.org/10.1177/0163443712449497)

3. \phantomsection \hypertarget{bib-bottom-up-economics}{}<a name="bib-bottom-up-economics"></a>
    [Redlich, T., & Moritz, M. (2016). Bottom-up Economics. Foundations of a Theory of Distributed and Open Value Creation. In The decentralized and networked future of value creation (pp. 27-57). Springer, Cham.](https://doi.org/10.1007/978-3-319-31686-4_3)

4. \phantomsection \hypertarget{bib-open-o-meter}{}<a name="bib-open-o-meter"></a>
    [J. Bonvoisin and R. Mies, "Measuring Openness in Open Source Hardware with the Open-o-Meter," *Procedia CIRP*, vol. 78, pp 388–393, Jan. 2018.](https://doi.org/10.1016/j.procir.2018.08.306)

5. \phantomsection \hypertarget{bib-the-source}{}<a name="bib-the-source"></a>
    [J. Bonvoisin, R. Mies, R. Stark, and J.-F. Boujut, "What is the 'Source' of Open Source Hardware?," *Journal of Open Hardware*, vol. 1, no. 1, p. 18, 2017.](http://doi.org/10.5334/joh.7)

6. \phantomsection \hypertarget{bib-ohw-principles}{}<a name="bib-ohw-principles"></a>
    [Open Source Hardware Association, "Open Source Hardware (OSHW) Statement of Principles 1.0," 2016. \[Online\]. Available: <http://www.oshwa.org/definition/>. \[Accessed: 30-Jun-2019\].](https://www.oshwa.org/definition/)

7. \phantomsection \hypertarget{bib-ohw-best-practices}{}<a name="bib-ohw-best-practices"></a>
    [Open Source Hardware Association, "Best Practices for Open-Source Hardware 1.0," 18-Apr-2013. \[Online\]. Available: <http://www.oshwa.org/sharing-best-practices/>. \[Accessed: 30-Mar-2016\].](http://www.oshwa.org/sharing-best-practices/)

8. \phantomsection \hypertarget{bib-os-definition}{}<a name="bib-os-definition"></a>
    [Open Source Initiative, "The Open Source Definition 1.9," 22-Mar-2007. \[Online\]. Available: <https://opensource.org/osd-annotated>. \[Accessed: 30-Mar-2016\].](https://opensource.org/osd-annotated)

9.  \phantomsection \hypertarget{bib-din-spec-3105-2}{}<a name="bib-din-spec-3105-2"></a>
    [DIN SPEC 3105‑2, *Open Source Hardware – Part 2: Community-based assessment*.](DIN_SPEC_3105-2.md)

10. \phantomsection \hypertarget{bib-cc-by-4}{}<a name="bib-cc-by-4"></a>
    [Creative Commons Corporation, "Attribution 4.0 International,"25-Nov-2013 \[Online\]. Available: <https://creativecommons.org/licenses/by/4.0/> \[Accessed: 31-Mar-2020\].](https://creativecommons.org/licenses/by/4.0/legalcode)

11. \phantomsection \hypertarget{bib-cc-by-sa-4}{}<a name="bib-cc-by-sa-4"></a>
    [Creative Commons Corporation, "Attribution ShareAlike 4.0 International," 25-Nov-2013 \[Online\]. Available: <https://creativecommons.org/licenses/by-sa/4.0/> \[Accessed: 31-Mar-2020\].](https://creativecommons.org/licenses/by-sa/4.0/legalcode)
    
12. \phantomsection \hypertarget{bib-gpl-3}{}<a name="bib-gpl-3"></a>
    [GNU Project, "GNU General Public Liense – Version 3", 29-Jun-2007 \[Online\]. Available: <https://www.gnu.org/licenses/gpl-3.0.en.html> \[Accessed: 31-Mar-2020\].](https://www.gnu.org/licenses/gpl-3.0.en.html)

13. \phantomsection \hypertarget{bib-ohl-2}{}<a name="bib-ohl-2"></a>
    [CERN, "Open Hardware License Version 2", 12-Mar-2020 \[Online\]. Available: <https://kt.cern/ohlv2> \[Accessed: 31-Mar-2020\].](https://www.ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)

14. \phantomsection \hypertarget{bib-fsf-definition}{}<a name="bib-fsf-definition"></a>
    [Free Software Foundation, Inc., "The Free Software Definition," 30-Jul-2019. \[Online\]. Available: <https://www.gnu.org/philosophy/free-sw.en.html> \[Accessed: 21-Aug-2019\].](https://www.gnu.org/philosophy/free-sw.en.html)

15. \phantomsection \hypertarget{bib-debian-fs-guidelines}{}<a name="bib-debian-fs-guidelines"></a>
    [The Debian Project, "Free Software Guidelines version 1.1", 26-Apr-2004 \[Online\]. Available: <https://www.debian.org/> \[Accessed: 31-Mar-2020\].](https://www.debian.org/social_contract#guidelines)

16. \phantomsection \hypertarget{bib-free-cultural-works-definition}{}<a name="bib-free-cultural-works-definition"></a>
    [Freedom defined moderators, "Definition of Free cultural Works version 1.1", 17-Feb-2015 \[Online\]. Available: <https://freedomdefined.org/Definition> \[Accessed: 31-Mar-2020\].](https://freedomdefined.org/Definition)

17. \phantomsection \hypertarget{bib-open-definition}{}<a name="bib-open-definition"></a>
    [Open Knowledge Foundation "The Open Definition version 2.1", Nov-2015 \[Online\]. Available: <https://opendefinition.org/> \[Accessed: 31-Mar-2020\].](https://opendefinition.org/od/2.1/en/)

18. \phantomsection \hypertarget{bib-open-definition}{}<a name="bib-vdi-4500"></a>
    [VDI 4500, *Technische Dokumentation*.](https://www.vdi.de/fileadmin/pages/vdi_de/redakteure/richtlinien/inhaltsverzeichnisse/9665043.pdf)

[^1]: Conversion from a file format to another (especially conversion
    from edition formats (e.g. docx) to export formats (e.g. pdf) is
    often bound to information losses. Converting a FreeCAD file into
    STEP is transforming a fully parametrized constraints- and
    feature-based model into a static network of points (mesh) and
    losing the ability to edit the shape that is represented by the
    model or the mesh.

[^2]: In this context, well-publicized means that, inter alia, patent
    offices worldwide will be able to find and retrieve the
    documentation via the internet.

[^3]: *Documentation* ([3.8](#38-documentation)) does not only facilitate hardware
    replication or "making". It also facilitates hardware operation,
    maintenance, repair, recycling, etc. This standard makes no
    difference between the relative importance of these activities and
    considers them as equal. Therefore, in this standard, the four
    rights of *open source hardware* ([3.3](#33-open-source-hardware)) are not restricted to the
    sole hardware production phase and cover the whole hardware life
    cycle.
