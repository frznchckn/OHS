# Repository of Open Hardware Standards

## Intro

This repository holds open source standards in the the field of open source hardware (OSH). Furthermore it can be (and has been) used for (further) development of those standards, report issues and give feedback.

## Naming

The documents

- DIN SPEC 3105-1
- DIN SPEC 3105-2
- OHS 3105-1
- OHS 3105-2

are adoptions of [DIN SPEC 3105-1](https://www.beuth.de/en/technical-rule/din-spec-3105-1/324805763) 
and [DIN SPEC 3105-2](https://www.beuth.de/en/technical-rule/din-spec-3105-2/324805750), 
respectively, by [DIN e.V.](https://www.din.de/en/din-and-our-partners/din-e-v) 
used under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode). 
This first publishing of an official standard under a free/open license is enabled 
by DIN's pilot project to approach open source communities and the great collaboration 
of lots of different actors and activists of the OSH community (huge thanks again!).

As indicated, you will find two mutually exclusive prefixes in file names in this repository:

- `DIN SPEC`\
  for adopted work whose content is identical with the original document
  - The files here are the original source files which then have been published by the licensor DIN e.V. with altered layout; the content itself however remained unchanged.
- `OHS` (Open Hardware Standard)\
  for adopted work with _changed_ content (e.g. after further development)
  - we use this alternative prefix in order to avoid confusion and trademark infringement.

## What is it?

**DIN SPEC 3105-1** delivers an unambiguous and operational definition of the
concept of open source hardware and breaks it down into objective
criteria for judging the compliance of a piece of hardware with this
definition.

**DIN SPEC 3105-2** defines requirements for implementing a community-based certification
procedure for open source hardware. It aims at groups or persons willing to build a
certification procedure as well as groups or persons willing to attest
the compliance of the documentation of a piece of hardware with
the requirements set in the DIN SPEC 3105-1.

This is the first standard published
by DIN e.V. under a free/open license. Following the principles of open
source, anybody can contribute to its further development online. Please do :) (following our [contribution guide](CONTRIBUTING.md)).

## Source & Export

The main (source) files and their generated versions:

* __DIN SPEC 3105-1__ ---
  [Source (Markdown)](DIN_SPEC_3105-1.md) ---
  [HTML](https://osegermany.gitlab.io/OHS/din-spec-3105-1/)
  /[alt.](https://osegermany.gitlab.io/OHS/DIN_SPEC_3105-1.html) ---
  [PDF](https://osegermany.gitlab.io/OHS/DIN_SPEC_3105-1.pdf)
* __DIN SPEC 3105-2__ ---
  [Source (Markdown)](DIN_SPEC_3105-2.md) ---
  [HTML](https://osegermany.gitlab.io/OHS/din-spec-3105-2/)
  /[alt.](https://osegermany.gitlab.io/OHS/DIN_SPEC_3105-2.html) ---
  [PDF](https://osegermany.gitlab.io/OHS/DIN_SPEC_3105-2.pdf)

---

## Get the documentation build-tool {#get-build-tool}

make sure you have the [MoVeDo](https://github.com/movedo/MoVeDo) doc build tool checked out:

If you have not yet cloned this repo, you can do so plus get MoVeDo in one go:

```bash
git clone --recurse-submodules https://github.com/OSEGermany/OHS.git
cd OHS
```

If you already have this repo locally, you can switch to it and get MoVeDo like this:

```bash
cd OHS
git submodule update --init
```

## How to generate output

If you have MoVeDo available locally as [described above](#get-build-tool),
simply run:

```bash
movedo/scripts/build
```

