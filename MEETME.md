---
# See `man pandoc | grep "Metadata variables"`
title: OHS 3105 - Meeting Minutes
# These two will be supplied their final value from `git describe`
subtitle: "version: <UNKNOWN>"
version: <UNKNOWN>
lang: en-US
charset: UTF-8
license: CC-BY-SA 4.0
keywords: Open source hardware, DIN, 3105, Meeting Minutes
subject: Meeting Minutes
category: Meeting Minutes
papersize: a4
geometry: "top=2cm,bottom=2cm,left=3cm,right=3cm"
---

## DIN SPEC Webcall 19.03.

Attendees:

- Martin Häuer (minutes)
- Lukas Winter
- Jerry de Vos
- Tobias Burkert

### status quo

- editing is going well; some delay in times of corona :)
- last issues can be fully resolved within the next days followed of final proofreading early next week
- printable TsDC-DB and TsDC-questionnaire created. Find them [here](https://gitlab.com/OSEGermany/oh-tsdc)
- **A GOOD TsDC-DB IS CRUCIAL NOW** if we don't have proper TsDC, we are bypassing the whole standard. What OSH is would be the personal opinion of each peer-reviewer.

### organisational

- I'll send the final documents via mail. Please read into them (**also TsDC please**) and give feedback (also regarding wording, spelling, formulations). **DEADLINE: 26.03.** (early afternoon, so I still have a chance to adjust the content)
    - feedback channel: preferrably the corresponding gitlab repo (issues); choose your own way how to reach me (Martin) otherwise
- Documents will be handed over to DIN on 27.03.

### discussion points

- constructive peer-reviewing only? (#26)
    - which is removing the additional weight for peer-verifications
    - approved; comment: negative peer-verifications are difficult to handle in practise, some people just never built a machine
- Freeze peer-reviewing after approval, unfreeze after reports? (#24)
    - approved
- Add definition of free/open-source license? (#23)
    - approved, add CC-BY & CC-BY SA as example
- permanent URLs (#22)
    - nothing yet (not from my research, not from DIN)
    - → wait a few days more + ask IT experts if there's a proper definition out there
    - → postpone issue otherwise
- rename DIN SPEC 3105-2 from "peer-certification" to "community-based certification"?
    - Background: all definitions including "peer" are crossed out / renamed (see issue #17)
    - DIN likes it, but didn't give me any recommendation so far
    - → approved: community-based certification; comment: fresh name and + more open (not limited to the meaning of peer-review in science; anyone can be a reviewer)


## DIN SPEC Webcall 10.03.

Attendees:

- Martin Häuer
- Lukas Schattenhofer (minutes)
- Robert Mies
- Jeremy Bonvoisin
- Tobias Wenzel
- Lukas Winter
- Emilio Velis

### organisational

- publishing rescheduled to mid-April
- standard to be sent to DIN ASAP; deadline 27.03.

### discussion points

- Life Cycle phases & TsDC (6, 15)
    - Life Cycle are kept in document but excluded from formal TsDC
    - The phases don't matter for the license anymore
    - TsDCs should give a solid base for the first phase and can be extended once we move towards the second or third phase after some experience on the certification procedure is gained.
    - The TsDCs should relate to the type of users and purpose the product is made for. 
    - Best practices examples might be useful to illustrate TsDCs.
    - Who decides on changes in the TsDCs?
        - There is a contribution guide for the DinSpec and will be adapted for the TsDCs.
    - TsDCs might become a standard on their own, since they will turn into very specific and specialiced document.
        - Maybe after a few years of TsDC development they will become stable enough to be incorporated directly in the DinSpec.
    - There will be another online meeting for discussing the TsDCs further. Monday or Tuesday (16.03. , 17.03.)
- add list of conform licenses as annex? (4)
    - Should this rather belong to the guideline?
    - Right now the DinSpec refers to OSWHA. If OSWHA does not list one of these licenses they might not be included by DINs standard as well.
- only OSS allowed in documentation? (3)
    - This is usually defined by the licenses used.
    - If software is mentioned in the documentation, it needs to be OSS.
        - As a part of the piece of hardware software should not stop the piece of hardware from being OSS.
    - Leads connecting proprietary parts to an open source piece of hardware?
        - Many useless products might arise from that. (Connecting Laptop and charger = OSH)
        - In optics there are complex products connecting only proprietary pieces of hardware and still should be considered OSH.
    - The certification body can still decide not to review useless products.
        - Can the body decide that?
    - Should the closed source software be mentioned or not?
        - No, because it points to one specific company.
        - yes, we list closed source hardware pieces in the documentation as well.
        - Conclusion: Let's not make it to difficult for the beginning.

### updates

- align to standard definitions (17)
- optional certification label (19)
    - The label should be moved to the annex but there is no need to use the label anymore.
    - The label might be created by the certification body itself.
    - DINs requirement: ....
- no more different levels of certification, preliminary stage removed (6, 8)

### optional discussion points

- Use stable external references to define the Life Cycle phases (12)
- actions after TsDC update required for OSH devs? (5)
    - No, when the law gets updated you don't get punished for what you've done.
    - Certification according to the year of publication
    - TsCDs requirements might become more strict.
    - There should be rules on the duration of validity of TsDCs, process of changing the TsDCs and transition period between two versions TsDCs.
        - The certification should be unlimited.
        - No in 10 years time opinions change drastically and that should be accounted for.
    - The discussion will be continued on the TsDCs meeting.

### conclusion

find the details on [gitlab](https://gitlab.com/OSEGermany/OHS/-/issues/)

important issues in **bold**
optional issues _italic_

| issue | subject | status |note|
| -------- | -------- | -------- | -------- |
| 19     | Certification Body shouldn't issue certificates _with_ a mark of conformity     | staged for change     |label will be optional and moves to annex|
|18|change referencing of defined terms|staged for change|no more capital letters|
|**17**|Align definitions to international standards|**open** |find definition for _certificate_
|16|correct label: "certified after DIN SPEC" → "certified according to DIN SPEC"|staged for change|"according to" is right
|**15**|Kick TsDC?|staged for change|see #6|
|14|Leave selection of peer reviewers to the certification body|pre-rejected|too detailed|
|13|Set recipients, remove choices|closed|redundant|
|12|Update OSI definition 1.0 → 1.9|staged for change|let's update
|11|Use stable external references to define the Life Cycle phases|**open**|need to check the standards|
|10|DIN SPEC 3105-2: remove time frame of 9 working days|staged for change|will be removed|
|9|clarify minimum to be OSH|pre-rejected|see #6|
|8|DIN SPEC 3105-2: remove preliminary state|staged for change|will be removed|
|7|design-specific requirements for 3rd Life Cycle phase|pre-rejected|out of scope, but unforgotten|
|**6**|missing requirements for 2nd & 3rd Life Cycle|**decision needed**|keep life cycle but exclude it from TsDC?
|5|actions after TsDC update required for OSH devs?|pre-rejected|no actions|
|4|(independent) license requirements|staged for change|add annex|
|3|add requirements on included software|staged for change|add note for software|
|2|all Life Cycle phases mandatory to be 100% open source?|pre-rejected|see #6|
